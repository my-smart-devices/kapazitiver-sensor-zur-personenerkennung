#define READ_PIN D0
#define OUT_PIN D5

const unsigned int avg_counts = 7;
const unsigned int threshold = 3000;

volatile long flank_counter = 0L;
unsigned long mov_avg_flanks[avg_counts];
unsigned int p = 0;

ICACHE_RAM_ATTR void incrementFlankCounter() {
  flank_counter += 1;
}

unsigned long movavg() {
  unsigned long s = 0L;
  for(int i = 0; i <avg_counts; i++){
    s += mov_avg_flanks[i];
  }
  return s / avg_counts;
}

void setup() {
  Serial.begin(115200);
  pinMode(D0, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(READ_PIN), incrementFlankCounter, RISING);
}

void loop() {
  Serial.print(flank_counter);
  Serial.print(" ");
  Serial.print(0);
  Serial.print(" ");
  unsigned long avg = movavg();
  Serial.print(avg);
  Serial.print(" ");
  Serial.println(5000);
  if (avg < threshold) {
    digitalWrite(OUT_PIN, HIGH);
  } else {
    digitalWrite(OUT_PIN, LOW);
  }
  mov_avg_flanks[p] = flank_counter;
  p = (p + 1) % avg_counts;
  flank_counter = 0L;
  delay(50);
}