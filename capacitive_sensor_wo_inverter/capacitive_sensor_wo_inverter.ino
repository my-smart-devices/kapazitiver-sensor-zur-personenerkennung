#define INV_OUT 0 //D3
#define READ_PIN 14 //D5
#define OUT_PIN LED_BUILTIN

unsigned long rise_time = 0L;
unsigned long duration = 0L;
unsigned long lastReset = 0L;
int last_flank_counter = 0;

const unsigned int avg_counts = 7;
const unsigned int threshold = 3300;

volatile long flank_counter = 0L;
unsigned long mov_avg_flanks[avg_counts];
unsigned int p = 0;

IRAM_ATTR void incrementFlankCounter()
{
  flank_counter += 1;
  duration += millis() - rise_time;
}

unsigned long movavg()
{
  unsigned long s = 0L;
  for (int i = 0; i < avg_counts; i++)
  {
    s += mov_avg_flanks[i];
  }
  return s / avg_counts;
}

void setup()
{
  Serial.begin(9600);

  pinMode(INV_OUT, OUTPUT);
  pinMode(OUT_PIN, OUTPUT);

  digitalWrite(OUT_PIN, HIGH);
  delay(300);
  digitalWrite(OUT_PIN, LOW);

  attachInterrupt(digitalPinToInterrupt(READ_PIN), incrementFlankCounter, RISING);
}

void loop()
{
  digitalWrite(INV_OUT, HIGH);
  rise_time = millis();
  digitalWrite(INV_OUT, LOW);

  if (lastReset + 100 < millis())
  {
    Serial.print(flank_counter);
    Serial.print(" ");
    Serial.print(70);
    Serial.print(" ");
    unsigned long avg = movavg();
    Serial.print(avg);
    Serial.print(" ");
    Serial.print(duration);
    Serial.print(" ");
    Serial.println(90);

    if (avg <= threshold)
    {
      digitalWrite(OUT_PIN, HIGH);
    }
    duration = 0L;

    mov_avg_flanks[p] = flank_counter;
    p = (p + 1) % avg_counts;
    flank_counter = 0L;
    lastReset = millis();
  }
  else
  {
    digitalWrite(OUT_PIN, LOW);
  }
}
