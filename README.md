# Kapazitiver Personen-Sensor

## Disclaimer

Das System ist aufgrund der verwendeten Bauart mit Breadboard, Jumperkabeln, etc. sehr störungsanfällig. Kleine Änderungen an der Umgebung (siehe Tabelle *Fehlerquellen*) können Auswirkungen auf den Threshold des Systems haben. Diese Störung wurde versucht soweit möglich in Hardware zu begegnen (siehe *Kondensatoren und "Antennen"*). Trotzdem treten Messungenauigkeiten und -schwankungen auf, die dann in Software gemittelt wurden (siehe *Software*).

Aufgrund beschriebener Probleme wurden zwei Ansätze verfolgt: mit und ohne Inverterbaustein. Hiervon war letztendlich nur der mit Inverterbaustein (auch aufgrund der Zeit) erfolgreich. Dennoch möchten wir den aktuellen Stand hier aufführen.

Das nachfolgende Dokument fasst Erfahrungen, Probleme und mögliche Lösungen, die während des Aufbaus auftraten, zusammen und hofft dadurch einen deterministischen Nachbau oder Verbesserung zu ermöglichen.

## Kapazitiver Sensor mit Inverterbaustein CD74HC04E

### Teileliste
- Arduino-MicrypControlle mit mind. 2 freie digital IO-Ports o.ä.
- LED (opt. mit eingebautem Widerstand)
- 6x Jumperkabel
- 100 $\mu$F Kondensator
- 100 nF Kondensator
- 56 k$\Omega$ Widerstand
- Breadboard
- Inverterbaustein, ~5nS Schaltzeit, für 5V geeignet

### Aufbau

![Aufbau 1](Layout_1_2.jpg)

### Beschreibung

#### LED
Die LED dient als Indikator, ob ein hochkapazitives Objekt (z.B. ein Mensch) in der Nähe ist. Aufgrund des Aufbaus dauert es ~0.5s, bis eine Annäherung detektiert wird. Der Aufbau ist durch die verbauten Kondensatoren so weit stabilisiert, dass die LED an bleiben soll(te), solange ein solches Objekt in der Nähe ist.

#### Kondensatoren und "Antennen"
Das Breadboard wird als Antenne genutzt. Auf diese Weise müssen Kapazitäten innerhalb des Breadboards nicht als Störung behandelt werden, sondern dienen der Stabilisierung und Erzeugung des Invertersignals.
Außerdem werden zwei Jumper-Kabel entfernt von dem Inverter in das Breadboard gesteckt. Diese dienen ebenfalls als Antenne zur räumlichen Erfassung eines kapazitiven Objekts (das Breadboard erfordert zur Erfassung die möglichst parallele Ausrichtung des Objekts). Der Abstand zur weiteren Schaltung ist notwendig, um Störungen zu reduzieren.
Sowohl die Antennen (100nF), als auch der Inverter-Eingang (100$\mu$F) wurden mit einem Kondensator mit GND verbunden, um die Kapazität des Systems zu erhöhen und Schwankungen (z.B. durch Luftfeuchtigkeit) auszugleichen. Würden diese nicht existieren, kann es vorkommen (tatsächlich relativ häufig), dass es zu so vielen Invertierungen und damit zu RISING-Triggern am arduino kommt, sodass das System abstürzt.

#### Software

Die Software ist in C mithilfe der Arduino-Library geschrieben worden.
Das Programm nutzt einen ISR RISING, um steigende Flanken und damit Invertierungen zu zählen.
Innerhalb der Loop Methode wird der entsprechende Counter alle 50ms zurückgesetzt (1. SW-Schritt zur Mittelung der Werte). Die letzten n-Werte (konfigurierbar über avg_counts) werden arithmetisch gemittelt. Unterschreitet dieser Wert dann einen konfigurierbaren Threshold wird dies als Begegnung mit einem kapazitiven Objekt gewertet und die LED geht an, solange dieser Wert unterhalb des Thresholds liegt (z.B. die Person vor dem Sensor steht).
Der Threshold muss entsprechend der eingesetzten Umgebung auf Basis von Messungen gesetzt werden. Die Window-Size zur Mittelung hat mit sieben gute Werte erzielt. Bei sehr störungsreichen Umgebungen (z.B. Wind, Neonröhren, o.ä.) kann der Wert weiter erhöht werden. Dies geht jedoch mit einer verzögerten Reaktion des Systems einher.

## Kapazitiver Sensor ohne Inverterbaustein

### Teileliste
- Arduino-MicrypControlle mit mind. 2 freie digitalIO-Ports o.ä.
- LED (opt. mit eingebautem Widerstand)
- 1x Jumperkabel

### Aufbau

Die LED wird zwischen GND und der definierten PIN (OUT_PIN) verbunden. Das Jumperkabel wird an INV_OUT und READ_PIN gesteckt.

### Beschreibung

Das System macht sich zu Nutze, dass zum detektieren einer 1 an dem READ_PIN ein gewisses Potential erreicht werden muss. Dieses Potential baut sich je nach Kapazität der Umgebung unterschiedlich schnell auf. Die Software erzeugt am INV_OUT ein HIGH Potential. Nun wird die zeitliche Differenz gemessen, bis am READ_PIN ebenfalls ein HIGH Potential anliegt. Hiernach wird am INV_OUT ein LOW Potential angelegt und diese zeitliche Ausbreitung gemessen. Aus der Änderung dieser Zeit kann ermittelt, ob ein kapazitives Objekt in der Nähe ist, oder nicht (je länger die Dauer zum Aufbau eines Potentials, desto mehr Kapazität in der Umgebung)

### Problem

Das System benötigt für die Abarbeitung des Codes zur Zeitmessung zu lange, um aussagekräftige Messungen durchführen zu können. Schaut man sich das Zeit-Potential-Verhalten in einem Logicanalyzer an kann man erkennen, dass das Prinzip funktioniert, aber nicht ausreichend genaue Messungen liefert.


## Allgemeine Fehlerquellen
|Fehlerquelle|Auswirkung|Behebung|
|---|---|---|
|Wind|Rauschen|Fehlerquelle entfernen o. reduzieren, Window-Size vergrößern|
|Neonröhren|
|Elektrische Geräte in der Nähe|
|Statische Aufladung|
|Erdung des kapazitiven Objekts|verhinderung Aufbau eines Potentials, keine Invertierung mehr|Erdung aufheben z.B. Fuß von der Heizung nehmen|
|Im Breadboard sind weitere metallische Teile eingestekct|der Flank counter weicht stark aufgrund der geänderten Kapazität des Systems von den voreingestellten Werten ab| Koppelkondensator + Werte neu einstellen (z.B. bei Logicanalyzern, Oszilloskopen, o.ä.), Werte neu anpassen, Breadboard + Schaltung schirmen|
